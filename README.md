# Excel to CSV Converter for Linux

This repository contains a simple Bash script to convert each sheet in an Excel file (.xlsx) to individual CSV files using csvkit. It's designed to help beginners work with Excel data on Linux systems and transition to using command-line tools for data processing.

## Prerequisites

Before you can use the script, you need to have csvkit installed on your Linux system. csvkit is a suite of command-line tools for converting to and working with CSV, the king of tabular file formats.

## Installing csvkit

On Debian-based systems (like Ubuntu), you can install csvkit from the official repositories:

```bash
sudo apt update
sudo apt install csvkit
```

##Making the Script Executable

After you clone or download the script from this repository, you need to make it executable.

This is done via the `chmod`command:

```bash
chmod +x xlsx2csv.sh

```

## Installing csvkit

The script is straightforward to use. Simply pass the Excel file as an argument to the script:


```bash
chmod +x xlsx2csv.sh

```

The script will then convert each sheet in the myexcel.xlsx file into a separate CSV file.

## Testing the Script

To verify the script is working correctly, you can use the provided test file `finance_data.xls`:

```bash
./xlsx2csv.sh test_file/finance_data.xls
```

This will demonstrate the script's functionality by converting the `finance_data.xls` file into CSV format, allowing you to ensure everything operates as expected.

## What the Script Does

Here's a step-by-step breakdown of what the script does:

1.Checks if you've provided an Excel file as an argument.
2.Confirms that the file exists.
3.Extracts the sheet names from the Excel file using in2csv.
3.For each sheet, it generates a safe file name for the CSV file.
4.Converts each sheet to a CSV file using in2csv.
5.Saves each CSV file in the current directory, appending the sheet name to the file name for identification.

##Output

The output CSV files will be named after the base name of your Excel file, followed by the sheet name, like so:

`myexcel_Sheet1.csv`
`myexcel_Sheet2.csv`
`myexcel_Sheet3.csv`
...


Feel free to customise the template further to match your project's specifics or additional details you'd like to include.

## Auhtor
Kalonji A. Tshisekedi

## License 

MIT License

Copyright (c) 2024 kalonji Tshisekedi

