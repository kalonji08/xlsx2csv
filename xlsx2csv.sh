#!/bin/bash

# Check if an argument was provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 path_to_excel_file.xlsx"
    exit 1
fi

# The Excel file passed as the first argument
excel_file=$1

# Check if the specified file exists
if [ ! -f "$excel_file" ]; then
    echo "File does not exist: $excel_file"
    exit 1
fi

# Get the base name of the file without the path and extension
base_name=$(basename "$excel_file" .xlsx)

# Use in2csv to list the sheet names and loop over them
sheet_names=$(in2csv -n "$excel_file")
for sheet in $sheet_names; do
    # Replace spaces with underscores and remove special characters for the filename
    safe_sheet_name=$(echo "$sheet" | tr ' ' '_' | tr -cd '[:alnum:]_')
    
    # Create the CSV file name
    csv_file="${base_name}_${safe_sheet_name}.csv"

    echo "Converting sheet '$sheet' to '$csv_file'"
    # Convert the sheet to CSV
    in2csv "$excel_file" --sheet "$sheet" > "$csv_file"
done

echo "Conversion complete."
